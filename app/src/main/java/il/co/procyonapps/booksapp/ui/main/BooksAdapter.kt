package il.co.procyonapps.booksapp.ui.main

import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import il.co.procyonapps.booksapp.R
import il.co.procyonapps.booksapp.data.BooksResponse
import kotlinx.android.synthetic.main.book_item.view.*

class BooksAdapter : RecyclerView.Adapter<BooksAdapter.BookHolder>() {

    var bookList = emptyList<BooksResponse.Book>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.book_item, parent, false)
        return BookHolder(view)
    }

    override fun getItemCount(): Int {
        return bookList.size
    }

    override fun onBindViewHolder(holder: BookHolder, position: Int) {
        holder.bind(bookList[position])
    }


    inner class BookHolder(val myView: View) : RecyclerView.ViewHolder(myView) {
        fun bind(book: BooksResponse.Book) {

            val colors = book.placeholderColor
            val backgroundColor = Color.rgb(colors.red, colors.green, colors.blue)

            val placholderDrawable = GradientDrawable().apply {
                shape = GradientDrawable.RECTANGLE
                color = ColorStateList.valueOf(backgroundColor)
            }

            Picasso.get().load(book.url).placeholder(placholderDrawable).into(myView.ivBookCover)



            myView.tvAuthor.text = book.body
            myView.tvTitle.text = book.title
        }
    }
}