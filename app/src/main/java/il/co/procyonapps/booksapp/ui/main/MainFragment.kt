package il.co.procyonapps.booksapp.ui.main

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import il.co.procyonapps.booksapp.MyApp
import il.co.procyonapps.booksapp.R
import il.co.procyonapps.booksapp.data.BooksResponse
import kotlinx.android.synthetic.main.main_fragment.*

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private  val viewModel: MainViewModel by lazy {
        ViewModelProvider
            .AndroidViewModelFactory.getInstance(requireActivity().application)
            .create(MainViewModel::class.java)
    }

    val booksAdapter by lazy {
        BooksAdapter()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        rvBooks.apply{
            layoutManager = LinearLayoutManager(context)
            adapter = booksAdapter
        }
        viewModel.booksList.observe(viewLifecycleOwner, Observer <List<BooksResponse.Book>?>{

            Log.d("MainFragment", "result: $it")
            it?.let{
                booksAdapter.bookList = it
            }

        })
    }

}
