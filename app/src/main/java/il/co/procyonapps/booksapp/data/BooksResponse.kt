package il.co.procyonapps.booksapp.data


import com.google.gson.annotations.SerializedName

data class BooksResponse(
    @SerializedName("data") val `data`: List<Book> = listOf()
) {
    data class Book(
        @SerializedName("body") val body: String = "",
        @SerializedName("placeholderColor") val placeholderColor: PlaceholderColor = PlaceholderColor(),
        @SerializedName("title") val title: String = "",
        @SerializedName("url") val url: String = ""
    ) {
        data class PlaceholderColor(
            @SerializedName("blue") val blue: Int = 0,
            @SerializedName("green") val green: Int = 0,
            @SerializedName("red") val red: Int = 0
        )
    }
}