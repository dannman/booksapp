package il.co.procyonapps.booksapp.ui.main

import android.app.Application
import androidx.lifecycle.*
import il.co.procyonapps.booksapp.data.BooksResponse
import il.co.procyonapps.booksapp.network.DataProvider
import kotlinx.coroutines.launch

class MainViewModel(val app: Application) : AndroidViewModel(app) {


    val booksList: LiveData<List<BooksResponse.Book>?> = liveData {
        val response = DataProvider().getListOfBooks(app.applicationContext)
        emit(response?.data)
    }
}
