package il.co.procyonapps.booksapp.network

import android.content.Context
import com.google.gson.Gson
import il.co.procyonapps.booksapp.data.BooksResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.IOException

class DataProvider {

    suspend fun getListOfBooks(context: Context): BooksResponse? = withContext(Dispatchers.IO) {

        val am = context.assets
        try {
            val inputStream = am.open("books.json")
            val size: Int = inputStream.available()
            val buffer = ByteArray(size)
            inputStream.read(buffer)
            inputStream.close()

            val rawJson = String(buffer, Charsets.UTF_8)

            return@withContext Gson().fromJson(rawJson, BooksResponse::class.java)
        } catch (e: IOException) {
            e.printStackTrace()
            return@withContext null
        }


    }
}